/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package practicaarray;

import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author dsouk
 */
public class Practicaarray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        arraypractica();
    }
    
    
   public static void arraypractica(){
    String numero = JOptionPane.showInputDialog("Introduce el tamaño de array");
    int n = Integer.parseInt(numero);
    
    int[] numeros =new int[n];
    
    int min =0;
    int max=10;
    
    for (int i=0; i<n;i++){
    
    numeros[i]=(int)Math.floor(Math.random()*(max-min+1)+min);
   
    }
   
    //System.out.println(Arrays.toString(numeros));  muestra el array para ver que numeros no estan 
    String numencontrar = JOptionPane.showInputDialog("Introduce el numero que quieres encontrar");
    int num = Integer.parseInt(numencontrar);
    int contador=0;
    boolean existe=false;
    for (int i=0; i<numeros.length;i++){
       if(numeros[i]==num){
           contador++;
            existe=true;
       }
    
    
    }
    
    if(existe==true){
      System.out.println(Arrays.toString(numeros));   
    System.out.println("Correcto, el numero de veces encontrado es : "+contador);
    }else{
    System.out.println("Fallo");
    }
    
   }
}
